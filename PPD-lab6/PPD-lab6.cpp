// PPD-lab6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <omp.h>
#include <vector>
#include "PPD-lab6.h"

using namespace std;

void init_vectors(vector<vector<int>>& vectors) {
	int vector_capacity = 100000;
	#pragma omp parallel for shared(vector_capacity,vectors)
	for (int i = 0;i < vectors.size();++i) {
		for (int j = 0;j < vector_capacity;++j) {
			vectors[i].push_back(j);
		}
	}
}
void print_vector(vector<int> v) {
	//for (unsigned int j = 0;j < v.size();++j) {
	//	//cout << "v[" << j << "] = " << v[j]<< " , ";
	//	cout << v[j] << " , ";
	//}
	//cout << endl;
}

void print_vectors(vector<vector<int>> vectors) {
	for (unsigned int i = 0; i < vectors.size();++i) {
		cout << " vector size " << vectors[i].size() << endl;
		print_vector(vectors[i]);
	}
}

void with_for(vector<vector<int>> vectors, int nrOfThreds) {
	omp_set_dynamic(0);
	omp_set_num_threads(nrOfThreds);//set number of threads to use
	int product = 1;
	vector<int> result(vectors[0].size());
	double start = omp_get_wtime();
	#pragma omp parallel shared(vectors,result) private(product)
	{
		#pragma omp for 
		for (int i = 0; i < vectors[0].size();++i) {
			product = 1;
			for (int j = 0;j < vectors.size();++j) {
				product = product*vectors[j][i];
			}
			result[i] = product;
		}
	}
	print_vector(result);
	int sum = 0;
	#pragma omp parallel for  reduction(+:sum)
	for (int i = 0;i < result.size();++i) {
		sum += result[i];
	}
	//cout << " sum is : " << sum <<endl;
	double end = omp_get_wtime();
	cout << " With parallel for time : " << end - start << endl;
}

void with_for_schedule_static(vector<vector<int>> vectors, int nrOfThreds,int chunk) {
	omp_set_dynamic(0);
	omp_set_num_threads(nrOfThreds);//set number of threads to use
	int product = 1;
	vector<int> result(vectors[0].size());
	double start = omp_get_wtime();
	#pragma omp parallel shared(vectors,result) private(product)
	{
		#pragma omp for schedule(static,chunk)
		for (int i = 0; i < vectors[0].size();++i) {
			product = 1;
			for (int j = 0;j < vectors.size();++j) {
				product = product*vectors[j][i];
			}
			result[i] = product;
		}
	}
	print_vector(result);
	int sum = 0;
	#pragma omp parallel for schedule(static,chunk) reduction(+:sum)
	for (int i = 0;i < result.size();++i) {
		sum += result[i];
	}
	//cout << " sum is : " << sum << endl;
	double end = omp_get_wtime();
	cout << " With parallel schedule static : " << end - start << endl;
}

void with_for_schedule_dynamic(vector<vector<int>> vectors, int nrOfThreds, int chunk) {
	omp_set_dynamic(0);
	omp_set_num_threads(nrOfThreds);//set number of threads to use
	int product = 1;
	vector<int> result(vectors[0].size());
	double start = omp_get_wtime();
#pragma omp parallel shared(vectors,result) private(product)
	{
#pragma omp for schedule(dynamic,chunk)
		for (int i = 0; i < vectors[0].size();++i) {
			product = 1;
			for (int j = 0;j < vectors.size();++j) {
				product = product*vectors[j][i];
			}
			result[i] = product;
		}
	}
	print_vector(result);
	int sum = 0;
#pragma omp parallel for schedule(dynamic,chunk) reduction(+:sum)
	for (int i = 0;i < result.size();++i) {
		sum += result[i];
	}
	//cout << " sum is : " << sum << endl;
	double end = omp_get_wtime();
	cout << " With parallel for schedule dynamic  : " << end - start << endl;
}

void with_for_schedule_guided(vector<vector<int>> vectors, int nrOfThreds, int chunk) {
	omp_set_dynamic(0);
	omp_set_num_threads(nrOfThreds);//set number of threads to use
	int product = 1;
	vector<int> result(vectors[0].size());
	double start = omp_get_wtime();
#pragma omp parallel shared(vectors,result) private(product)
	{
#pragma omp for schedule(guided,chunk)
		for (int i = 0; i < vectors[0].size();++i) {
			product = 1;
			for (int j = 0;j < vectors.size();++j) {
				product = product*vectors[j][i];
			}
			result[i] = product;
		}
	}
	print_vector(result);
	int sum = 0;
#pragma omp parallel for schedule(guided,chunk) reduction(+:sum)
	for (int i = 0;i < result.size();++i) {
		sum += result[i];
	}
	//cout << " sum is : " << sum << endl;
	double end = omp_get_wtime();
	cout << " With parallel for schedule guided : " << end - start << endl;
}

void with_sections(vector<vector<int>> vectors, int nrOfThreds) {
	omp_set_dynamic(0);
	omp_set_num_threads(nrOfThreds);//set number of threads to use
	vector<int> result(vectors[0].size());
	int imp = vectors[0].size() / 8;
	//cout << imp << endl;
	double start = omp_get_wtime();
	#pragma omp parallel sections shared(result)
	{
		#pragma omp section
		{
			for (int i = 0;i < imp;++i) {
				int product = 1;
				for (int j = 0;j < vectors.size();++j) {
					product = product*vectors[j][i];
				}
				result[i] = product;
			}
		}
		#pragma omp section
		{
			for (int i = imp;i < imp + imp;++i) {
				int product = 1;
				for (int j = 0;j < vectors.size();++j) {
					product = product*vectors[j][i];
				}
				result[i] = product;
			}
		}
		#pragma omp section
		{
			for (int i = 2 * imp;i < (2 * imp) + imp;++i) {
				int product = 1;
				for (int j = 0;j < vectors.size();++j) {
					product = product*vectors[j][i];
				}
				result[i] = product;
			}
		}
		#pragma omp section
		{
			for (int i = 3 * imp;i < (3 * imp) + imp;++i) {
				int product = 1;
				for (int j = 0;j < vectors.size();++j) {
					product = product*vectors[j][i];
				}
				result[i] = product;
			}
		}
		#pragma omp section
		{
			for (int i = 4 * imp;i < (4 * imp) + imp;++i) {
				int product = 1;
				for (int j = 0;j < vectors.size();++j) {
					product = product*vectors[j][i];
				}
				result[i] = product;
			}
		}
		#pragma omp section
		{
			for (int i = 5 * imp;i < (5 * imp) + imp;++i) {
				int product = 1;
				for (int j = 0;j < vectors.size();++j) {
					product = product*vectors[j][i];
				}
				result[i] = product;
			}
		}
		#pragma omp section
		{
			for (int i = 6 * imp;i < (6 * imp) + imp;++i) {
				int product = 1;
				for (int j = 0;j < vectors.size();++j) {
					product = product*vectors[j][i];
				}
				result[i] = product;
			}
		}
		#pragma omp section
		{
			for (int i = 7 * imp;i < vectors[0].size();++i) {
				int product = 1;
				for (int j = 0;j < vectors.size();++j) {
					product = product*vectors[j][i];
				}
				result[i] = product;
			}
		}
	}
	print_vector(result);
	int sum = 0;
	#pragma omp parallel for  reduction(+:sum)
	for (int i = 0;i < result.size();++i) {
		sum += result[i];
	}
	//cout << " sum is : " << sum << endl;
	double end = omp_get_wtime();
	cout << " With sections time : " << end - start << endl;
}

void with_for_coalesced(vector<vector<int>> vectors, int nrOfThreds) {
	omp_set_dynamic(0);
	omp_set_num_threads(nrOfThreds);//set number of threads to use
	int product = 1;
	vector<int> result(vectors[0].size());
	int sum = 0;
	omp_set_nested(1);
	double start = omp_get_wtime();
	#pragma omp parallel shared(vectors,result,sum) private(product)
	{
		#pragma omp for 
		for (int i = 0; i < vectors[0].size();++i) {
			product = 1;
			for (int j = 0;j < vectors.size();++j) {
				product = product*vectors[j][i];
			}
			result[i] = product;
		}
		#pragma omp for reduction(+:sum)
		for (int i = 0;i < result.size();++i) {
			sum += result[i];
		}
	}
	print_vector(result);
	//cout << " sum is : " << sum << endl;
	double end = omp_get_wtime();
	cout << " With parallel for coalesced time : " << end - start << endl;
}

int main()
{
	int n = 10;
	vector<vector<int>> vectors(n);
	int nrOfThreads = 4;
	init_vectors(vectors);
	int chunk = 10;
	//print_vectors(vectors);
	with_for(vectors, nrOfThreads);
	with_for_coalesced(vectors, nrOfThreads);
	with_sections(vectors, nrOfThreads);
	with_for_schedule_static(vectors, nrOfThreads, chunk);
	with_for_schedule_dynamic(vectors, nrOfThreads, chunk);
	with_for_schedule_guided(vectors, nrOfThreads, chunk);
	cout << "Hello world " << endl;
	getchar();
    return 0;
}

